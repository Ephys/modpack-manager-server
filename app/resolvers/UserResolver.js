// @flow

import { stringValidator } from '@foobarhq/validators';
import User from '../models/User';
import * as Hasher from '../modules/hasher';
import ValidationError from '../modules/ValidationError';
import { GraphQl } from '../services/http/decorators/index';
import { emailValidator } from '../modules/validators';
import { generateAuthToken } from '../modules/jwt';
import { authUser } from '../modules/auth';
import { AccessDeniedError } from '../modules/TokenError';
import type { ResolverContext } from '../services/http/Types';

export default class ChainManagerResolver {

  @GraphQl('Query.user', {
    session: {
      user: authUser,
    },
  })
  static async getUserByUuid({ uuid, user }: { uuid: string, user: User }) {

    const foundUser = await User.findOne({ where: { uuid } });
    if (!foundUser) {
      return null;
    }

    if (!await user.canReadEntity(foundUser)) {
      return null;
    }

    return foundUser;
  }

  @GraphQl('Query.authUser', {
    parameters: {
      email: stringValidator(),
      password: stringValidator({ trim: false }),
    },
  })
  static async authUser(
    { email, password }: { email: string, password: string },
    context: ResolverContext,
  ): Promise<?User> {

    const user: User = await User.findOne({ where: { email: email.toLocaleLowerCase() } });
    if (!user) {
      return null;
    }

    const passwordValid = await Hasher.verifyAgainst(password, user.password);
    if (!passwordValid) {
      return null;
    }

    if (context) {
      // mark the newly authentified user as the active user
      // even though they did not provide a JWToken.
      authUser.set(context, user);
    }

    return user;
  }

  @GraphQl('Query.authenticatedUser', {
    session: {
      user: authUser,
    },
  })
  static getAuthenticatedUser({ user }: { user: User }): Promise<User> {
    return user;
  }

  @GraphQl('User.jwt', {
    session: {
      loggedUser: authUser,
    },
  })
  static getUserJwt({ user, loggedUser }: { loggedUser: User, user: User }) {

    if (user.id !== loggedUser.id) {
      throw new AccessDeniedError('You can only access your own JWT', 'ERR_ACCESS_INVALID');
    }

    return generateAuthToken({
      user: user.uuid,
    });
  }

  @GraphQl('User.roles')
  static getUserRoles({ user }: { user: User }) {
    return user.getRoles();
  }

  @GraphQl('Mutation.createUser', {
    parameters: {
      email: emailValidator(),
      password: stringValidator({ trim: false, minLength: 6 }),
    },
  })
  static async createUser({ email, password }: { email: string, password: string }): Promise<User> {
    email = email.toLocaleLowerCase();

    const user: User = await User.findOne({
      where: {
        email,
      },
    });

    if (user != null) {
      throw new ValidationError({ key: 'email', message: 'DUPLICATE' });
    }

    return User.create({
      email,
      password: await Hasher.hash(password),
    });
  }
}
