import { GraphQLScalarType, Kind } from 'graphql';
import validateUuid from 'uuid-validate';

function parseDate(value: string): ?Date {
  const timestamp = Date.parse(value);

  if (Number.isNaN(timestamp)) {
    return null;
  }

  return new Date(timestamp);
}

const DateType = new GraphQLScalarType({
  name: 'Date',
  description: 'ISO 8601 Date type',
  parseValue: parseDate,
  serialize(value: Date) {
    return value.toISOString();
  },
  parseLiteral(ast) {
    switch (ast.kind) {
      case Kind.INT:
        return Number(ast.value);

      case Kind.STRING:
        return parseDate(ast.value);

      default:
        return null;
    }
  },
});

function parseUuid(string: string): ?string {
  if (!validateUuid(string)) {
    return null;
  }

  return string;
}

const UuidType = new GraphQLScalarType({
  name: 'Uuid',
  description: 'UUID v4 format',
  parseValue: parseUuid,
  serialize(value: String): String {
    return value;
  },
  parseLiteral(ast) {

    switch (ast.kind) {
      case Kind.STRING:
        return parseUuid(ast.value);

      default:
        return null;
    }
  },
});

// id ASC, p DESC
function parseOrder(string: string): ?Array<Array<string>> {
  const orders = string
    .replace(/\s+/g, ' ')
    .split(',')
    .map(order => {
      const orderArray = order.trim().split(' ').map(str => str.trim());
      orderArray[1] = orderArray[1].toUpperCase();

      if (orderArray.length !== 2) {
        return null;
      }

      if (orderArray[1] !== 'ASC' && orderArray[1] !== 'DESC') {
        return null;
      }

      return orderArray;
    });

  if (orders.includes(null)) {
    return null;
  }

  return orders;
}

const ListOrderType = new GraphQLScalarType({
  name: 'ListOrder',
  description: 'Format: <field> <direction ASC|DESC>[,...]',
  serialize(value: Array<Array<string>>): string {
    return value.map(val => val.join(' ')).join(', ');
  },
  parseValue: parseOrder,
  parseLiteral(ast) {

    switch (ast.kind) {
      case Kind.STRING:
        return parseOrder(ast.value);

      default:
        return null;
    }
  },
});

export default {
  Date: DateType,
  Uuid: UuidType,
  ListOrder: ListOrderType,
};
