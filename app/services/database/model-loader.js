// @flow

import fs from 'mz/fs';
import { getDbMeta } from './decorators';

export async function loadModels(modelPath) {
  const models = (await fs.readdir(modelPath)).map(fileName => {
    const descriptor = require(`${modelPath}/${fileName}`); // eslint-disable-line global-require

    return descriptor.default || descriptor;
  });

  for (const model of models) {
    if (model.disabled) {
      continue;
    }

    const meta = getDbMeta(model);
    if (meta && meta.relationships) {
      for (const relationship of meta.relationships) {
        // eslint-disable-next-line prefer-spread
        model[relationship.name].apply(model, relationship.args);
      }
    }
  }
}
