// @flow

const METADATA = Symbol('db-metadata');

type DbMetaStruct = {
  postRegister: ?Function,
  relationships: ?SequelizeRelationship[],
};

type SequelizeRelationship = {
  name: string,
  args: any[],
};

function getSetMeta(target): DbMetaStruct {
  if (!target[METADATA]) {
    target[METADATA] = Object.create(null);
  }

  return target[METADATA];
}

export function getDbMeta(target): ?DbMetaStruct {
  return target[METADATA];
}

function makeRelationDecorator(name) {
  return function createRelation(...args) {
    return function decorateRelation(Model) {
      const meta = getSetMeta(Model);
      meta.relationships = meta.relationships || [];

      meta.relationships.push({
        name,
        args,
      });

      return Model;
    };
  };
}

export const BelongsTo = makeRelationDecorator('belongsTo');
export const BelongsToMany = makeRelationDecorator('belongsToMany');
export const HasMany = makeRelationDecorator('hasMany');
