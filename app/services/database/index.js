// @flow

import path from 'path';
import { URL } from 'url';
import Sequelize from 'sequelize';
import config, { ready as configReady } from '../config';
import logger from '../logger';
import { loadModels } from './model-loader';

// eslint-disable-next-line import/no-mutable-exports
export let sequelize = null;

export const ready = configReady.then(async () => {
  const uri = new URL(config.databaseUri);

  sequelize = new Sequelize(
    uri.pathname.substr(1),
    uri.username,
    uri.password,
    {
      host: uri.hostname,
      port: uri.port,
      dialect: uri.protocol.slice(0, -1),
      logging: process.env.MGCS_DB_DEBUG ? logger.debug.bind(logger) : null,
    },
  );

  const modelDirectory = path.normalize(`${__dirname}/../../models`);
  await loadModels(modelDirectory);

  await sequelize.authenticate();

  logger.debug('Connection success');

  await sequelize.sync();
});

export async function resetDatabase() {
  if (process.env.NODE_ENV !== 'development') {
    throw new Error('You are trying to reset the production database. This is not allowed.');
  }

  await ready;
  logger.info('Resetting database');
  await sequelize.sync({ force: true });
  logger.info('Database has been reset.');
}
