// @flow

import { set as setProperty, camelCase } from 'lodash';
import logger from '../../logger';
import type { GraphQlFunction, ResolverBuilderOptions } from './types';
import { addAuth } from './auth';
import { addValidation } from './validation';

const Meta = Symbol('graphql-meta');
type MetaStruct = Map<string, ResolverBuilderOptions>;

function getSetMeta(Class): MetaStruct {
  if (!Class[Meta]) {
    Class[Meta] = new Map();
  }

  return Class[Meta];
}

function getMeta(Class): ?MetaStruct {
  return Class[Meta];
}

// $FlowBug
export function GraphQl(schemaPath: string, options?: GraphQlOptions = {}) {
  return function applyGraphQlDecorator(Class: Function, method: string) {
    const t = Object.assign({}, options);
    t.method = method;

    const meta: MetaStruct = getSetMeta(Class);

    if (meta.has(schemaPath)) {
      throw new TypeError(`GraphQL path "${schemaPath}" has been defined twice in resolver ${Class.name}`);
    }

    meta.set(schemaPath, t);
  };
}

// eslint-disable-next-line no-extra-parens
const protoKeyWhitelist = Object.getOwnPropertyNames((class {
}).prototype);

export function classToResolvers(Class: Function | Object): Object {
  if (typeof Class !== 'function') {
    return Class;
  }

  for (const key of Object.getOwnPropertyNames(Class.prototype)) {
    if (!protoKeyWhitelist.includes(key)) {
      logger.warn(`Class ${JSON.stringify(Class.name)} has instance properties. Please remove property ${JSON.stringify(key)}`);
    }
  }

  const meta: ?MetaStruct = getMeta(Class);
  if (!meta) {
    return Class;
  }

  const resolvers = {};

  meta.forEach((options: ResolverBuilderOptions, key: string) => {

    // $FlowBug
    const method = methodToResolver(Class, options);

    setProperty(resolvers, key, method);
  });

  return resolvers;
}

function methodToResolver(Class: Function, options: ResolverBuilderOptions): GraphQlFunction {

  let method: GraphQlFunction = normalizeFunction(Class, Class[options.method]); // this wrapper function will be executed last
  method = addAuth(method, options);
  method = addValidation(method, options);

  return method;
}

function normalizeFunction(Class: Function, method: Function): GraphQlFunction {

  return function resolver(parent, parameters, context, meta) {

    if (parent) {
      // TODO the same method can have multiple different parents. Add a config option to overwrite parentName ?
      const parentName = camelCase(meta.parentType.name);
      parameters[parentName] = parent;
    }

    context.meta = meta;

    return method.call(Class, parameters, context);
  };
}
