// @flow

import passport from 'passport';
import type { GraphQlFunction, ResolverBuilderOptions } from './types';

const EMPTY_OBJECT = {};
Object.freeze(EMPTY_OBJECT);

export function addAuth(method: GraphQlFunction, options: ResolverBuilderOptions): GraphQlFunction {
  if (!options.session) {
    return method;
  }

  const sessionKeys = options.session ? Object.getOwnPropertyNames(options.session) : null;

  if (!sessionKeys || sessionKeys.length === 0) {
    return method;
  }

  return async function auth(parent, parameters, context, meta) {
    const jwt = await getRequestJwt(context);

    // if (!jwt) {
    //   throw TokenError.invalidSignature();
    // }

    const jwtItems = jwt ? jwt.items : EMPTY_OBJECT;
    await Promise.all(sessionKeys.map(async sessionKey => {
      const fetcher = options.session[sessionKey];

      parameters[sessionKey] = await fetcher(jwtItems, context);
    }));

    return method(parent, parameters, context, meta);
  };
}

const jwtCacheSymbol = Symbol('jwt-cache');
function getRequestJwt(context) {

  // The same request can asked to authenticate multiple times, depending on requested resources.
  // Avoid parsing the Auth header each time.
  if (context[jwtCacheSymbol]) {
    return context[jwtCacheSymbol];
  }

  return new Promise((resolve, reject) => {
    passport.authenticate('jwt', (err, jwt) => {
      if (err) {
        return void reject(err);
      }

      context[jwtCacheSymbol] = jwt;

      resolve(jwt);
    })(context.request);
  });
}
