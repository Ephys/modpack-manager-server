// @flow

import type { ResolverContext } from '../Types';

export type GraphQlOptions = {|
  parameters?: RouteValidators,
  session?: RouteValidators,
|};

export type ResolverBuilderOptions = GraphQlOptions & { method: string };

export type GraphQlFunction = (
  parent: ?any,
  parameters: { [key: string]: any },
  req: ResolverContext,
  meta: Object
) => any;

export type RouteValidators = {
  [key: string]: Function,
};
