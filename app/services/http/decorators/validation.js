// @flow

import { structValidator, InvalidData } from '@foobarhq/validators';
import ValidationError from '../../../modules/ValidationError';
import type { GraphQlFunction, ResolverBuilderOptions } from './types';

export function addValidation(method: GraphQlFunction, options: ResolverBuilderOptions): GraphQlFunction {
  if (!options.parameters) {
    return method;
  }

  const validator = structValidator(options.parameters);

  return function validateSchema(obj, input, req, meta) {
    try {
      input = validator(input);
    } catch (e) {
      if (e instanceof InvalidData) {
        throw new ValidationError({ key: e.key || meta.fieldName, message: e.reason });
      }

      throw e;
    }

    return method(obj, input, req, meta);
  };
}
