// @flow

import path from 'path';
import restify from 'restify';
import passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';
import { makeExecutableSchema } from 'graphql-tools';
import { maskErrors } from 'graphql-errors';
import { fileLoader, mergeTypes, mergeResolvers } from 'merge-graphql-schemas';
import { graphqlRestify, graphiqlRestify } from 'apollo-server-restify';
import packageJson from '../../../package.json';
import config, { ready as configReady } from '../config';
import logger from '../logger';
import { classToResolvers } from './decorators';
import { classToRouter } from './rest-decorators';

export const ready = configReady.then(() => {
  const server = restify.createServer({
    title: packageJson.name,
  });

  server.use(restify.plugins.bodyParser());
  server.use(restify.plugins.queryParser());

  const graphqlTypes = mergeTypes(fileLoader(path.join(__dirname, '../../graphql'), { recursive: true }));

  const resolvers = fileLoader(path.join(__dirname, '../../resolvers'), { recursive: true });

  const graphqlResolvers = mergeResolvers(resolvers.map(classToResolvers));

  const schema = makeExecutableSchema({
    typeDefs: graphqlTypes,
    resolvers: graphqlResolvers,
  });

  maskErrors(schema);

  passport.use(new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
    secretOrKey: config.api.jwtSecret,
  }, (payload, done) => done(null, payload)));

  function graphqlParams(request) {
    return {
      schema,
      context: { request },
    };
  }

  server.get('/graphql', graphqlRestify(graphqlParams));
  server.post('/graphql', graphqlRestify(graphqlParams));
  server.get('/graphiql', graphiqlRestify({ endpointURL: '/graphql' }));

  for (const resolver of resolvers) {
    const router = classToRouter(resolver);

    if (router) {
      router.applyRoutes(server, '/api');
    }
  }

  // todo don't print rest api url if no route uses rest
  // todo don't print graphql api url if no route uses graphql
  server.listen(config.api.port, () => {
    logger.info(`graphql  started on http://localhost:${config.api.port}/graphql`);
    logger.info(`rest api started on http://localhost:${config.api.port}/api`);
  });
});
