import Sequelize from 'sequelize';

export type ResolverContext = {
  transaction?: ?Sequelize.Transaction,
};

export const defaultContext: ResolverContext = {};

Object.freeze(defaultContext);
