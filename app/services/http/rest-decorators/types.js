// @flow

import type { RouteValidators } from '../decorators/types';

export type ResolverFunction = (parameters: { [key: string]: any }, req: Request, meta: Object) => any;
export type ExpressFunction = (Object, Object, Function) => void;
export type Route = {
  httpMethod: string,
  path: string,
  method: string,

  pathParams: RouteValidators,
};

export type RouteOptions = {
  path?: RouteValidators,
};
