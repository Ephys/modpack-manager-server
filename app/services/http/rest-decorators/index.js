// @flow

import assert from 'assert';
import { Router } from 'restify-router';
import type { ExpressFunction, ResolverFunction, Route, RouteOptions } from './types';
import { addPathValidation } from './validation';

const Meta = Symbol('rest-meta');
type MetaStruct = Set<Route>;

function getSetMeta(Class): MetaStruct {
  if (!Class[Meta]) {
    Class[Meta] = new Set();
  }

  return Class[Meta];
}

function getMeta(Class): ?MetaStruct {
  return Class[Meta];
}

export const GET = makeHttpMethod('get');

function makeHttpMethod(httpMethod): Function {
  return function decorator(path, options: RouteOptions = {}): Function {
    return function applyRestDecorator(Class: Function, method: string) {
      const meta: MetaStruct = getSetMeta(Class);

      meta.add({
        httpMethod,
        path,
        method,
        pathParams: options.path,
      });
    };
  };
}

export function classToRouter(resolver: Function) {
  const meta = getMeta(resolver);

  if (!meta) {
    return null;
  }

  const router = Router();
  for (const routeData of meta) {

    /*
    export type ResolverFunction = (parameters: { [key: string]: any }, req: Request, meta: Object) => any;
     */

    let classMethod: ResolverFunction = resolver[routeData.method].bind(resolver);
    classMethod = addPathValidation(classMethod, routeData);

    // register route
    router[routeData.httpMethod](routeData.path, adaptToExpress(classMethod));
  }

  return router;
}

/**
 * Handles an async function to match restify's non-async requirements.
 *
 * @param {!Function} asyncFunction - The function to restify.
 * @returns {!Function} the restified function.
 */
function adaptToExpress(asyncFunction: ResolverFunction): ExpressFunction {
  assert.ok(asyncFunction instanceof Function, '[ServerUtil#asyncToRestify] parameter is not a function');

  return function expressToResolverAdapter(request, response, next) {
    try {
      const result = asyncFunction({}, request, {
        response, next, request,
      });

      if (result && result.then) {
        result
          .then(body => response.send(body))
          .catch(e => next(e));
      } else {
        response.send(result);
      }
    } catch (e) {
      next(e);
    }
  };
}
