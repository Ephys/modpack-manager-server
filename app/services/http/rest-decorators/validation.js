// @flow

import { structValidator, InvalidData } from '@foobarhq/validators';
import ValidationError from '../../../modules/ValidationError';
import type { ResolverFunction, Route } from './types';

export function addPathValidation(method: ResolverFunction, options: Route): ResolverFunction {
  if (!options.pathParams) {
    return method;
  }

  const pathValidator = structValidator(options.pathParams);

  return function validatePath(parameters, req, meta) {
    try {
      const pathParams = pathValidator(req.params);

      Object.assign(parameters, pathParams);
    } catch (e) {
      if (e instanceof InvalidData) {
        throw new ValidationError({ key: e.key || meta.fieldName, message: e.reason });
      }

      throw e;
    }

    return method(parameters, req, meta);
  };
}
