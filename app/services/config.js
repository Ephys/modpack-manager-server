// @flow

import fs from 'fs';
import path from 'path';
import buildConfig, { description } from 'json-config-generator';
import {
  structValidator as struct,
  numberValidator as number,
  stringValidator as string,
} from 'json-config-generator/lib/validators';
import findFile from 'find-file-up';
import { merge } from 'lodash';

function port(opts) {
  return number({
    integer: true,
    min: 1024,
    max: 65535,
    ...opts,
  });
}

const schema = struct({
  api: struct({
    jwtSecret: string({ [description]: 'Secret to use to sign Json Web Tokens' }),
    port: port({
      [description]: 'Port to use for the HTTP server',
    }),
  }),
  hashRounds: number({
    integer: true,
    unsigned: true,
    [description]: 'Please read the doc on rounds: https://www.npmjs.com/package/bcrypt',
  }),
  logDirectory: string({
    defaultValue: path.resolve(`${__dirname}/../../.app`),
    allowNull: true,
  }),
  databaseUri: string({
    [description]: '<dialect>://<username>:<password>@<hostname>:<port>/<dbname>',
  }),
});

/**
 * Returns the parsed contents of the package.json file.
 *
 * @returns {!Promise.<!Object>} The contents of package.json.
 */
function getPackageJson() {
  return new Promise((resolve, reject) => {
    findFile('package.json', '.', (err1, fp) => {
      if (err1) {
        return reject(err1);
      }

      return fs.readFile(fp, (err2, contents) => {
        if (err2) {
          return reject(err2);
        }

        return resolve(JSON.parse(contents));
      });
    });
  });
}

const env = process.env.NODE_ENV || 'production';
const Config: Object = {
  env,
  debug: env === 'development',
};

export const ready = Promise.all([
  buildConfig({
    useEnv: Boolean(process.env.MGCS_ENV_CONFIG),
    envPrefix: 'MGCS_',
    file: `.app/config_${process.env.NODE_ENV || 'production'}.json`,
    schema,
  }),
  getPackageJson(),
]).then(([newConfig, pkg]) => {
  merge(Config, newConfig);
  Config.pkg = pkg;
});

export default Config;
