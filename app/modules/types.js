// @flow

import type { Transaction } from 'sequelize';

type Order = 'ASC' | 'DESC';

export type SequelizeRelationGet = {
  transaction?: Transaction,
  order?: [string, Order][],
  where?: Object,
};

export type Period = {
  start: ?Date,
  end: ?Date,
};
