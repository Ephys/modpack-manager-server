import bcrypt from 'bcrypt';
import config from '../services/config';

export function verifyAgainst(password, aHash) {
  return bcrypt.compare(password, aHash.toString());
}

export function hash(password) {
  return bcrypt.hash(password, config.hashRounds);
}
