// @flow

import { typeof Model, Op } from 'sequelize';
import { merge, cloneDeep } from 'lodash';
import deepFreeze from 'deep-freeze-strict';
import ValidationError from './ValidationError';

/**
 * @module graphql-pagination
 *
 * An implementation of the Relay Cursor Connections Specification
 * @link https://facebook.github.io/relay/graphql/connections.htm
 */

type OrderTuple = [string, 'DESC' | 'ASC'];

export type Pagination = {|
  first: number,
  last: number,
  after: ?string,
  before: ?string,
  order: Array<OrderTuple>,
|};

export type PageInfo = {|
  hasPreviousPage: boolean,
  hasNextPage: boolean,
|};

export type Connection<NodeType> = {|
  edges: Edge<NodeType>[],
  nodes: NodeType[],
  pageInfo: PageInfo,
|};

export const EMPTY_CONNECTION: Connection<any> = {
  edges: [],
  nodes: [],
  pageInfo: {
    hasPreviousPage: false,
    hasNextPage: false,
  },
};

deepFreeze(EMPTY_CONNECTION);

export type Edge<NodeType> = {|
  node: NodeType,
  cursor: String,
|};

type FactoryConfig = {|
  model: Model,
  defaultLimit?: number,
  maxLimit?: number,
  defaultOrder?: OrderTuple,
  orderWhitelist?: string[],
|};

type ModelFinder = (query: Object) => Promise<Object[]>;

type SearchFunction = (
  pagination: Pagination,
  baseQuery?: Object,
  modelFinder?: ModelFinder,
) => Promise<Connection<Model>>;

export function modelSearchFactory(config: FactoryConfig): SearchFunction {

  const { model, defaultOrder = ['createdAt', 'ASC'], orderWhitelist = ['createdAt'], defaultLimit = 100, maxLimit = 40 } = config;

  Object.freeze(defaultOrder);
  Object.freeze(orderWhitelist);

  const modelFindAll = query => model.findAll(query);

  return async function search(pagination: Pagination, baseQuery?: Object = {}, findAll?: ModelFinder = modelFindAll) {

    if (pagination.first != null && pagination.last != null) {
      throw new ValidationError({
        key: 'pagination',
        message: 'Cannot have both pagination.first and pagination.last',
      });
    }

    const originalOrders = pagination.order ? cloneDeep(pagination.order) : [];
    if (originalOrders.length === 0) {
      originalOrders.push(cloneDeep(defaultOrder));
    }

    for (const orderItem of originalOrders) {
      if (!orderWhitelist.includes(orderItem[0])) {
        throw new ValidationError({ key: 'pagination.order', message: `Cannot sort by field ${JSON.stringify(orderItem[0])}.` });
      }
    }

    const requestedLimit = pagination.first || pagination.last;
    const query: Object = merge({
      limit: Math.min(maxLimit, requestedLimit) || defaultLimit,
      order: originalOrders,
    }, baseQuery);

    if (pagination.after != null) {
      const after = await model.findOne({ where: { uuid: pagination.after } });

      if (!after) {
        throw new ValidationError({ key: 'pagination.after', message: 'Entity does not exist.' });
      }

      merge(query, {
        where: {
          id: { [Op.gt]: after.id },
        },
      });
    }

    if (pagination.before != null) {
      const before = await model.findOne({ where: { uuid: pagination.before } });

      if (!before) {
        throw new ValidationError({ key: 'pagination.before', message: 'Entity does not exist.' });
      }

      merge(query, {
        where: {
          id: { [Op.lt]: before.id },
        },
      });
    }

    let results;
    if (pagination.last != null) {
      // we need to reverse the order so we select the last items instead of the first
      query.order = reverseOrder(query.order);

      results = await findAll(query);
      results.reverse();
    } else {
      // no need to reverse, fallback to simple version
      results = await findAll(query);
    }

    const edges = results.map(result => {
      return {
        node: result,
        cursor: result.uuid,
      };
    });

    return {
      edges,
      nodes: results,
      pageInfo: {
        hasNextPage: false,
        hasPreviousPage: false,
      },
    };
  };
}

function reverseOrder(order) {
  if (!order) {
    return order;
  }

  return order.map(orderPart => {
    const direction = orderPart[1] === 'ASC' ? 'DESC' : 'ASC';

    return [orderPart[0], direction];
  });
}
