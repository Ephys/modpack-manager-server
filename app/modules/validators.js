// @flow

import isUuidV4 from 'uuid-validate';
import { InvalidData, optionalValidator, stringValidator, andValidator, enumValidator } from '@foobarhq/validators';
import isEmail from 'validator/lib/isEmail';

export function emailValidator() {
  return andValidator([
    stringValidator(),
    function validateEmail(input) {
      if (!isEmail(input)) {
        throw new InvalidData('Not an email');
      }

      return input;
    },
  ]);
}

const _uuidV4Validator = andValidator([
  stringValidator(),
  function validateUuidV4(input) {
    if (!isUuidV4(input)) {
      throw new InvalidData('Not a UUID v4');
    }

    return input;
  },
]);

export function uuidV4Validator(options) {
  if (!options) {
    return _uuidV4Validator;
  }

  return optionalValidator(_uuidV4Validator, options);
}

export function languageValidator() {
  // TODO language code validation
  return stringValidator({
    trim: true,
    minLength: 2,
    maxLength: 5,
  });
}

export function uppercaseEnumValidator(enumValues) {
  return andValidator([toUpper, enumValidator(enumValues)]);
}

function toUpper(str: string) {
  if (typeof str === 'string') {
    return str.toUpperCase();
  }

  return str;
}
