// @flow

import { UserError } from 'graphql-errors';

export default class TokenError extends UserError {

  constructor(msg: string) {
    super(`Token Error: ${msg}`);
  }

  static invalidTokenPart(tokenPart: string, reason?: string = '') {
    return new TokenError(`Invalid token part [${JSON.stringify(tokenPart)}]. ${reason}`);
  }

  static invalidSignature() {
    return new TokenError('Invalid Token');
  }
}

export class AccessDeniedError extends UserError {
  constructor(msg: string, key: string) {
    super(`Access Denied: ${msg} ${key}`);
  }
}
