// @flow

import util from 'util';
import jwt from 'json-web-token';
import config from '../services/config';

const encodeJwt = util.promisify(jwt.encode);

export function generateAuthToken(items: { [key: string]: string }) {

  return encodeJwt(config.api.jwtSecret, { items });
}
