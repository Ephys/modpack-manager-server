// @flow

import User from '../models/User';
import type { ResolverContext } from '../services/http/Types';
import TokenError from './TokenError';

const authUserCache = Symbol('authUserCache');
export async function authUser(jwtSession: Object, context: ResolverContext): Promise<User> {
  if (context[authUserCache]) {
    return context[authUserCache];
  }

  const uuid = jwtSession.user;

  const manager: User = await User.findOne({ where: { uuid } });

  if (!manager) {
    throw TokenError.invalidTokenPart('user');
  }

  context[authUserCache] = manager;

  return manager;
}

/**
 * Manually updating who is the logged user, usually called right after a login or register
 * or any action that should authentify the created entity.
 * @param {!Request} context - The current request.
 * @param {!User} user - The new logged user.
 */
authUser.set = function setAuthUser(context: ResolverContext, user: User) {
  context[authUserCache] = user;
};

export async function authAdmin(jwtSession: Object, context: ResolverContext): Promise<User> {
  const user: User = await authUser(jwtSession, context);

  if (!user.isAdmin()) {
    throw TokenError.invalidTokenPart('user', 'Missing role ADMIN');
  }

  return user;
}
