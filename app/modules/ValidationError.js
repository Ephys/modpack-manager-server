import { UserError } from 'graphql-errors';

export default class ValidationError extends UserError {
  constructor(errors) {
    if (!Array.isArray(errors)) {
      errors = [errors];
    }

    const errorMap = errors.reduce((result, error) => {
      if (Object.prototype.hasOwnProperty.call(result, error.key)) {
        result[error.key].push(error.message);
      } else {
        result[error.key] = [error.message];
      }

      return result;
    }, {});

    super(errorMap);
  }
}
