// @flow

import { Model, DataTypes } from 'sequelize';
import { Options, Attributes } from 'sequelize-decorators';
import { sequelize } from '../services/database';

const EMAIL_LENGTH = 255;

const ROLES = {
  USER: 0b00,
  ADMIN: 0b01,
};

const ROLE_KEYS = Object.keys(ROLES);

Object.freeze(ROLES);

@Options({
  sequelize,
  tableName: 'mmm_users',
  freezeTableName: true,
  underscored: true,
})
@Attributes({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  email: {
    type: DataTypes.STRING(EMAIL_LENGTH),
    allowNull: false,
    unique: true,
    validate: {
      isEmail: true,
    },
  },
  password: {
    type: DataTypes.STRING(512).BINARY,
    allowNull: true,
  },
  roles: {
    type: DataTypes.INTEGER,
    allowNull: false,
    defaultValue: 0,
  },
  uuid: {
    type: DataTypes.UUID,
    allowNull: false,
    unique: true,
    defaultValue: DataTypes.UUIDV4,
  },
  createdAt: { type: DataTypes.DATE, field: 'created_at' },
  updatedAt: { type: DataTypes.DATE, field: 'updated_at' },
})
export default class User extends Model {

  static EMAIL_LENGTH = EMAIL_LENGTH;
  static ROLES = ROLES;

  id: number;
  uuid: string;
  email: string;
  password: string;
  roles: number;

  /**
   * Returns true if the identity (PK and type) of otherEntity is the same as this one.
   *
   * @param {!*} otherEntity - The other entity.
   * @returns {!boolean} Both entities are the same.
   */
  identityEquals(otherEntity: any) {
    if (otherEntity == null || typeof otherEntity !== 'object') {
      return false;
    }

    if (!otherEntity.constructor) {
      return false;
    }

    return otherEntity.constructor === User && otherEntity.get('id') === this.get('id');
  }

  isAdmin() {
    return this.hasRole(ROLES.ADMIN);
  }

  hasRole(role: $Values<typeof ROLES>) {
    return (this.get('roles') & role) === role;
  }

  getRoles() {
    const roles = [];

    for (const roleName of ROLE_KEYS) {
      const roleFlag = ROLES[roleName];

      if (this.hasRole(roleFlag)) {
        roles.push(roleName);
      }
    }

    return roles;
  }

  addRole(role: $Values<typeof ROLES>) {
    this.set('roles', this.get('roles') | role);
  }

  removeRole(role: $Values<typeof ROLES>) {
    this.set('roles', this.get('roles') & (0b11 ^ role));
  }

  canReadEntity(entity: Model): Promise<boolean> {
    if (!entity.isReadableBy) {
      return Promise.resolve(false);
    }

    return entity.isReadableBy(this);
  }

  isReadableBy(user: User): Promise<boolean> {
    return Promise.resolve(user.isAdmin() || user.id === this.id);
  }
}
