// @flow

import logger from './services/logger';
import { ready as configReady } from './services/config';
import { ready as databaseReady } from './services/database';
import './services/http';

process.on('unhandledRejection', err => {
  console.error('An unhandledRejection occurred');
  console.error(err);
  process.exit(1);
});

export default async function initApp() {
  await configReady;
  await databaseReady;

  logger.info('Application ready.');
};
