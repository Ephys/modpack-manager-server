-- DB: modpackmanager

-- ---
-- Table 'users'
-- ---

DROP TABLE IF EXISTS users CASCADE;

CREATE SEQUENCE users_id_seq;

CREATE TABLE users (
  id INTEGER NOT NULL DEFAULT nextval('users_id_seq'),
  email VARCHAR NOT NULL,
  password TEXT NOT NULL,
  PRIMARY KEY (id)
);

ALTER SEQUENCE users_id_seq OWNED BY users.id;

-- ---
-- Table 'mods'
-- ---

DROP TABLE IF EXISTS mods CASCADE;

CREATE SEQUENCE mods_id_seq;

CREATE TABLE mods (
  id INTEGER NOT NULL DEFAULT nextval('mods_id_seq'),
  label VARCHAR(255) NULL DEFAULT NULL,
  name VARCHAR(255) NOT NULL,
  comment TEXT NOT NULL DEFAULT '',
  authors VARCHAR(255) NULL DEFAULT NULL,
  homepage VARCHAR(255) NOT NULL,
  repository VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (id)
);

ALTER SEQUENCE mods_id_seq OWNED BY mods.id;

-- ---
-- Table 'mod_mc_versions'
-- ---

DROP TABLE IF EXISTS mod_mc_versions CASCADE;

CREATE SEQUENCE mod_mc_versions_id_seq;

CREATE TABLE mod_mc_versions (
  id INTEGER NOT NULL DEFAULT nextval('mod_mc_versions_id_seq'),
  mod_id INTEGER NOT NULL,
  minecraft_version VARCHAR(20) NOT NULL ,
  latest_mod_version VARCHAR(255) NOT NULL,
  download_link TEXT NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (mod_id, minecraft_version)
);

ALTER SEQUENCE mod_mc_versions_id_seq OWNED BY mod_mc_versions.id;

-- ---
-- Table 'modpacks'
-- ---

DROP TABLE IF EXISTS modpacks CASCADE;

CREATE SEQUENCE modpacks_id_seq;

CREATE TABLE modpacks (
  id INTEGER NOT NULL DEFAULT nextval('modpacks_id_seq'),
  owner INTEGER NOT NULL,
  PRIMARY KEY (id)
);

ALTER SEQUENCE modpacks_id_seq OWNED BY modpacks.id;

-- ---
-- Table 'modpack_mods'
-- ---

DROP TABLE IF EXISTS modpack_mods CASCADE;

CREATE TABLE modpack_mods (
  modpack_id INTEGER NOT NULL,
  mod_id INTEGER NOT NULL,
  used_mod_version VARCHAR(255) NOT NULL,
  PRIMARY KEY (modpack_id, mod_id)
);

-- ---
-- Table 'mod_requires'
-- ---

DROP TABLE IF EXISTS mod_requires CASCADE;

CREATE TABLE mod_requires (
  mod_version_id INTEGER NOT NULL,
  dependency_id INTEGER NOT NULL,
  PRIMARY KEY (mod_version_id, dependency_id)
);

-- ---
-- Foreign Keys
-- ---

ALTER TABLE mod_mc_versions ADD FOREIGN KEY (mod_id) REFERENCES mods (id);
ALTER TABLE modpacks ADD FOREIGN KEY (owner) REFERENCES users (id);
ALTER TABLE modpack_mods ADD FOREIGN KEY (modpack_id) REFERENCES modpacks (id);
ALTER TABLE modpack_mods ADD FOREIGN KEY (mod_id) REFERENCES mod_mc_versions (id);
ALTER TABLE mod_requires ADD FOREIGN KEY (mod_version_id) REFERENCES mod_mc_versions (id);
ALTER TABLE mod_requires ADD FOREIGN KEY (dependency_id) REFERENCES mod_mc_versions (id);
